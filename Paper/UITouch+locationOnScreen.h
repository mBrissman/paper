//
//  UITouch+locationOnScreen.h
//  Water Ball
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITouch (locationOnScreen)

- (CGPoint)locationOnScreen;

@end
