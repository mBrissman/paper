//
//  MBPoint.h
//  Paper
//
//  Created by Marcus Brissman on 11/14/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    MBBrush    = 0,
    MBCrayon   = 1
} MBDrawTool;

@interface MBPoint : NSObject

@property (nonatomic) CGPoint point;
@property (nonatomic) float scale;
@property (nonatomic) MBDrawTool drawTool;
@property (nonatomic) ccColor3B color;

- (void)drawPoint;

+ (id)pointWithPoint:(CGPoint)point;

@end
