//
//  MBPageLayer.m
//  Paper
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright 2012 Marcus Brissman. All rights reserved.
//

#import "MBPageLayer.h"
#import "UITouch+locationOnScreen.h"
#import "MBPoint.h"

@implementation MBPageLayer
{
    CCRenderTexture *canvas;
    CCRenderTexture *canvasTexture;
    CCSprite *brush;
    
    BOOL isLining;
    CGPoint startPoint;
    CGPoint endPoint;
    
    CCNode *lines;
    
    float currentSize;
    BOOL sizeDown;
    
    double previousTimeStamp;
    
    CGSize windowSize;
    
    // Colors
    CCMenuItemImage *redColorItem;
    CCMenuItemImage *greenColorItem;
    CCMenuItemImage *blueColorItem;
    
    ccColor3B currentColor;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    startPoint = [touch locationOnScreen];
    
    previousTimeStamp = [[NSDate date] timeIntervalSince1970];
    
    return YES;
}

//- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
//{
////    if (sizeDown) {
////        currentSize--;
////        if (currentSize == 7) {
////            sizeDown = NO;
////        }
////    } else {
////        currentSize++;
////        if (currentSize == 30) {
////            sizeDown = YES;
////        }
////    }
//    
//
//    endPoint = [touch locationOnScreen];
//    [self paintSpace];
//    
//    // New start point
//    startPoint = endPoint;
//    
//    //[self paintAtPoint:[touch locationOnScreen]];
//}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    double now = [[NSDate date] timeIntervalSince1970];
    double timeDif = now - previousTimeStamp;
    previousTimeStamp = now;
    
    
	CGPoint start = [touch locationInView: [touch view]];
	start = [[CCDirector sharedDirector] convertToGL: start];
	CGPoint end = [touch previousLocationInView:[touch view]];
	end = [[CCDirector sharedDirector] convertToGL:end];
    
	// begin drawing to the render texture
	[canvas begin];
    
	// for extra points, we'll draw this smoothly from the last position and vary the sprite's
	// scale/rotation/offset
	float distance = ccpDistance(start, end);
    
    double currentSpeed = distance / timeDif;
    
    float goalSize = currentSpeed / 2000;
    
    float size = currentSize;
    
   // NSLog(@"Speed: %f", currentSpeed);
	if (distance > 1)
	{
		int d = (int)distance;
        
        float sizeDifPerPoint = (goalSize - currentSize) / d;
        
        
		for (int i = 0; i < d; i++)
		{
			float difx = end.x - start.x;
			float dify = end.y - start.y;
			float delta = (float)i / distance;
            size = currentSize + i * sizeDifPerPoint;
            
            if(size > 1.0f)
                size = 1.0f;
            if(size < 0.1f)
                size = 0.0f;
            
            MBPoint *point = [MBPoint pointWithPoint:ccp(start.x + (difx * delta), start.y + (dify * delta))];
            [point setDrawTool:MBCrayon];
            [point setScale:size];
            [point setColor:currentColor];
            
            [point drawPoint];
		}
	}
    
    
    currentSize = size;
    
	// finish drawing and return context back to the screen
	[canvas end];
}

- (void)paintAtPoint:(CGPoint)point
{
    if (!lines) {
        lines = [CCNode node];
        [self addChild:lines];
    }
    CCSprite *newPoint = [CCSprite spriteWithFile:@"Brush.png"];
    newPoint.position = point;
    newPoint.scale = currentSize / 10;
    newPoint.color = ccc3(255, 0, 0);
    [lines addChild:newPoint];
    
    //			[brush setPosition:ccp(start.x + (difx * delta), start.y + (dify * delta))];
    ////			[brush setRotation:rand()%360];
    ////			float r = ((float)(rand()%50)/50.f) + 0.25f;
    //			//[brush setScale:r];
    //
    //            float newScale = (distance / 20.0f);
    //           // newScale = 1 / newScale;
    //            // NSLog(@"New scale: %f", newScale);
    //            if (newScale > 1) {
    //                newScale = 1;
    //            } else if(newScale < 0.5) {
    //                newScale = 0.5;
    //            }
    //
    //            [brush setScale: newScale];
    //
    //            brush.color = currentColor;
    //            // Call visit to draw the brush, don't call draw..
    //			[brush visit];
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    isLining = NO;
    currentSize = 0.05;
}

- (void)mixColors:(id)sender
{
    if (sender == redColorItem) {
        currentColor = ccc3(255, 0, 0);
    }
    if (sender == greenColorItem) {
        currentColor = ccc3(0, 255, 0);
    }
    if (sender == blueColorItem) {
        currentColor = ccc3(0, 0, 255);
    }
}

- (void)fillPolygon
{
    MBPoint *p1 = [MBPoint pointWithPoint:CGPointMake(120, 120)];
    MBPoint *p2 = [MBPoint pointWithPoint:CGPointMake(240, 120)];
    MBPoint *p3 = [MBPoint pointWithPoint:CGPointMake(240, 240)];
    MBPoint *p4 = [MBPoint pointWithPoint:CGPointMake(120, 240)];
    
    NSArray *arrayWithPoint = @[p1, p2, p3, p4];
    
    // Converts an array with MBPoint's to an c array
    int count = [arrayWithPoint count];
    CGPoint points[4];
    
    for (int i = 0; i < count; ++i) {
        points[i] = [[arrayWithPoint objectAtIndex:i] point];
    }
    
    // Do stuff with the C-array
    NSLog(@"%f %f", points[0].x, points[0].y);
    
    int arraySize = sizeof(points) / sizeof(CGPoint) ;
    
    
    
    [canvas begin];
    
    ccDrawColor4F(0, 0, 0, 1); 

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords );
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, arraySize, points);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, arraySize, points);
    
    glDrawArrays(GL_TRIANGLES, 0, count);
    
    [canvasTexture end];

}

- (id)init
{
    self = [super init];
    if (self) {
        
        windowSize = [[CCDirectorIOS sharedDirector] winSize];
        
        // Canvas
        canvas = [CCRenderTexture renderTextureWithWidth:windowSize.width height:windowSize.height];
        [canvas setPosition:ccp(windowSize.width / 2, windowSize.height / 2)];
        [self addChild:canvas z:1];
        
        CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)];
        [self addChild:colorLayer z:0];
        
        // Brush
        brush = [CCSprite spriteWithFile:@"SpecialBrush.png"];
		[brush setBlendFunc: (ccBlendFunc) { GL_ONE, GL_ONE_MINUS_SRC_ALPHA }];
		[brush setOpacity:100];
        currentColor = ccc3(255, 0, 0);
        
        currentSize = 0.05;
        
        // Color menu
        
        redColorItem = [CCMenuItemImage itemWithNormalImage:@"Color.png" selectedImage:@"Color.png" target:self selector:@selector(mixColors:)];
        redColorItem.color = ccc3(255, 0, 0);
        redColorItem.position = ccp(redColorItem.position.x - 200, redColorItem.position.y);
        
        greenColorItem = [CCMenuItemImage itemWithNormalImage:@"Color.png" selectedImage:@"Color.png" target:self selector:@selector(mixColors:)];
        greenColorItem.color = ccc3(0, 255, 0);

        blueColorItem = [CCMenuItemImage itemWithNormalImage:@"Color.png" selectedImage:@"Color.png" target:self selector:@selector(mixColors:)];
        blueColorItem.color = ccc3(0, 0, 255);
        blueColorItem.position = ccp(blueColorItem.position.x + 200, blueColorItem.position.y);
        
        CCMenu *menu = [CCMenu menuWithItems:redColorItem, greenColorItem, blueColorItem, nil];
        menu.position = ccp(windowSize.width / 2, 120);
        
        [self addChild:menu z:3];
        
        // TEST
        //[self fillPolygon];
        
        [[[CCDirectorIOS sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    }
    return self;
}

+ (CCScene *) scene
{
	CCScene *scene = [CCScene node];
    [scene addChild: [self node]];
    
	return scene;
}

- (void)paintSpace
{
    float x1 = startPoint.x;
    float y1 = startPoint.y;
    float x2 = endPoint.x;
    float y2 = endPoint.y;
    
    float a = (y2 - y1) / (x2 - x1);    // Angle
    float b = y1 - (a * x1);            // Y-value when x is 0
    float c = b / -a;                   // X-value when y is 0
    

    if (abs(y2 - y1) < abs(x2 - x1)) {
        
        // X-Loop
        float startX;
        float stopX;
        
        if (x1 < x2) {
            startX = x1;
            stopX = x2;
        } else {
            startX = x2;
            stopX = x1;
        }

        for (float x = startX; x < stopX; x = x + 3) {
            float y = a * x + b;
            if (y - floorf(y) < 3) {
                [self paintAtPoint:CGPointMake(x, floorf(y))];
            } else if (y - floorf(y) > 3) {
                [self paintAtPoint:CGPointMake(x, floorf(y) + 1)];
            }
        }
        
    } else {
        
        // Y-Loop
        float startY;
        float stopY;
        
        if (x1 < x2) {
            startY = y1;
            stopY = y2;
        } else {
            startY = y2;
            stopY = y1;
        }
        
        for (float y = startY; y < stopY; y = y + 3) {
            float x = c + y / a;
            if (x - floorf(x) < 3) {
                [self paintAtPoint:CGPointMake(floorf(x), y)];
            } else if (x - floorf(x) > 3) {
                [self paintAtPoint:CGPointMake(floorf(x) + 1, y)];
            }
        }
    }
}

@end
