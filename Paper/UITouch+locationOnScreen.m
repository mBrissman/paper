//
//  UITouch+locationOnScreen.m
//  Water Ball
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import "UITouch+locationOnScreen.h"
#import "cocos2d.h"

@implementation UITouch (locationOnScreen)

- (CGPoint)locationOnScreen
{
    CGPoint location = [[CCDirector sharedDirector] convertToGL:[self locationInView:[self view]]];
    return location;
}

@end
