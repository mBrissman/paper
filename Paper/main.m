//
//  main.m
//  Paper
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBAppDelegate class]));
    }
}
