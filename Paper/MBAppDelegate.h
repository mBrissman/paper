//
//  MBAppDelegate.h
//  Paper
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
