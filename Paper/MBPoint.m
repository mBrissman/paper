//
//  MBPoint.m
//  Paper
//
//  Created by Marcus Brissman on 11/14/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import "MBPoint.h"

@implementation MBPoint
{
    CCRenderTexture *canvas;
}
- (void)drawPoint
{
    CCSprite *sprite;
    switch ([self drawTool]) {
        case MBCrayon:
            
            sprite = [CCSprite spriteWithFile:@"Crayon.png"];
            [sprite setRotation:rand()%360];
            [sprite setBlendFunc: (ccBlendFunc) { GL_ONE, GL_ONE_MINUS_SRC_ALPHA }];
            [sprite setOpacity:20];
            
            break;
            
        default:
            
            sprite = [CCSprite spriteWithFile:@"Brush.png"];
            break;
    }
    
    [sprite setPosition:_point];  
    [sprite setScale: _scale];
    [sprite setColor:_color];

    [sprite visit];
}

+ (id)pointWithPoint:(CGPoint)point
{
    return [[self alloc] initWithPoint:point];
}

- (id)initWithPoint:(CGPoint)point
{
    self = [super init];
    if (self) {
        _point = point;
    }
    return self;
}

- (id)init
{
    return [self initWithPoint:CGPointMake(0, 0)];
}

@end
