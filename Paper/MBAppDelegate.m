//
//  MBAppDelegate.m
//  Paper
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright (c) 2012 Marcus Brissman. All rights reserved.
//

#import "MBAppDelegate.h"
#import "cocos2d.h"
#import "MBPageLayer.h"

@implementation MBAppDelegate
{
    CCDirectorIOS *director;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Creates the director
    director = (CCDirectorIOS *)[CCDirector sharedDirector];

    
    CGRect viewFrame = CGRectMake(0, 0, _window.bounds.size.height, _window.frame.size.width);

    
	// Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:viewFrame
								   pixelFormat:kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    
    [director setView:glView];
    [director setWantsFullScreenLayout:YES];
    [director setAnimationInterval:1.0/60];
    [director pushScene: [MBPageLayer scene]];
    
    [self.window setRootViewController:director];
    
    [self.window makeKeyAndVisible];
    return YES;
}

@end
