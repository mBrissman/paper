//
//  MBPageLayer.h
//  Paper
//
//  Created by Marcus Brissman on 11/13/12.
//  Copyright 2012 Marcus Brissman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MBPageLayer : CCLayer
    
+(CCScene *) scene;

@end
